package saad.main.WordProb;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class WordNet {
	private String lemma;
	
	public void findLemma() throws IOException{  
        //Document doc = Jsoup.connect("http://compling.hss.ntu.edu.sg/omw/cgi-bin/wn-gridx.cgi?usrname=&gridmode=wnbahasa").get();
        Document hasil = Jsoup.connect("http://compling.hss.ntu.edu.sg/omw/cgi-bin/wn-gridx.cgi?usrname=&gridmode=wnbahasa")
       		 .data("lemma", lemma)
       		 .data("lang", "zsm")
       		 .data("lang2", "eng")
       		 .post();
        for (Element table : hasil.select("table")) {
            for (Element row : table.select("tr")) {
                //Elements tds = row.select("td");
                System.out.println(row.text());   
            }
        }
	 }
	
	public String getLemma(){
		return this.lemma;
	}
	
	
}
