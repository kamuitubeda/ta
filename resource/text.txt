Jadi gini, sebenarnya dalam pertemuan kemarin malam itu teman-teman yg diundang ke pusat mau klarifikasi beberapa hal selain masalah agenda ke depan.

Beberapa hal yang perlu diklarifikasi:
1. Masalah undangan dari pusat
Jadi, teman-teman yang diundang ke pusat itu murni dipilih oleh teman-teman pusat. Untuk parameter penilaiannya teman-teman yang diundang pun sebenarnya kurang tahu pasti. Tapi sepertinya keaktifan adalah faktor penilaian paling utama. Soalnya Mbak Gisa tiap kali ada kumpul dan kegiatan selalu menanyakan siapa saja yg datang kepada Fristi. Jadi, saya rasa ke depan akan sangat bagus jika tiap pertemuan dibuat absensi. Masa seperti anggota DPR gak mau diabsen?

2. Masalah jaringan komunikasi yang nggak jalan.
Kalau untuk yang satu ini saya kami (Saya, Imam, Adha, dan Fristi) mohon maaf sebesar-besarnya. Karena 2 hari setelah pertemuan pertama di Dewandaru dulu kami memang ditunjuk oleh Mbak Anggun sebagai penyambung info dari pusat ke Surabaya. Dan kami merasa penyebaran informasi adalah salah satu masalah yg memang perlu dicari solusinya. Untuk saat ini, jaringan komunikasi yg telah disepakati adalah berdasarkan area kampus dan profesi. Kalau memang ternyata masih belum maksimal, mungkin akan dicarikan solusi lain lagi.

Kalau masalah info gathering relawan nasional itu karena kami memang baru ditelfon oleh relawan pusat pada hari Selasa. Dan waktu itu kami diminta mencari transport sendiri sebelum akhirnya mendapatkan info pada hari Rabu malam atau Kamis pagi (saya lupa pastinya) bahwa teman-teman pusat mau membantu masalah transport dan akomodasi untuk teman-teman yg diundang.

Jadi, sekali lagi, kami mohon maaf kalau kami tidak mengadakan kumpul sebelum kami berangkat ke Jakarta karena waktunya memang sangat mepet. Untuk undangan secara khusus sendiri memang tidak ada info. Teman-teman yg diundang itu langsung ditelfon. Info yang disebar melalui email semata-mata hanya undangan bersifat umum dan tidak disertai bantuan transportasi dan akomodasi dari pusat. Sekian.

3. Masalah Dana.
Kalau untuk masalah ini, sebenarnya semuanya sudah jelas. Tidak ada aliran dana sama sekali dari pusat untuk operasional kegiatan di Surabaya. Semuanya murni dari donasi teman-teman sekalian.

4. Masalah Rekening
Kalau masalah rekening, maaf, turuntangan bukan organisasi nir laba. Jadi sepertinya tidak perlu membuat rekening segala. Pengalaman saya selama menjadi relawan di beberapa gerakan sosial pun sama. Mereka tidak punya rekening dan hanya menggunakan rekening salah satu relawan sebagai rekening donasi.

5. Pelaporan kegiatan dan dana
Ini sebenarnya menjadi bahasan ketika di Jakarta. Dan teman-teman sempat mengusulkan kepada teman-teman di pusat agar membuat standar laporan kegiatan yg bisa digunakan secara nasional. Selama ini di semua daerah hampir sama, tidak ada pelaporan yg jelas. Karena memang tidak ada standar laporan yg jelas untuk setiap kegiatan yg sudah dilakukan. Dan di Surabaya sendiri pelaporan dilakukan di setiap pertemuan. Jadi, mohon maaf kalau ada info tentang pelaporan yg tidak tersampaikan kepada teman-teman yg berhalangan hadir.

Setelah mempertimbangkan banyak hal, maka diputuskan untuk membuat blog sebagai media informasi. Ke depan, setiap rencana kegiatan, agenda, laporan hasil kegiatan, laporan penggunaan dana, dokumentasi kegiatan, dan lain sebagainya yang berkaitan dengan kegiatan Relawan TURUNTANGAN Surabaya akan dipublikasikan di blog ini. Alamat blognya bisa diakses di http://turuntangansby.wordpress.com (masih dalam pengembangan dan pengisian konten awal)

------
Untuk agenda terdekat minggu ini adalah meramaikan wisuda UNESA dengan membuka stand serta melakukan kegiatan penggalangan dana (mekanisme penggalangan dana yang sudah disepakati: jualan bunga). Untuk masalah perizinan dan yg lain sebagainya masih diusahakan.

Kalau memang tidak ada halangan, Mas Anies akan datang di Surabaya tanggal 8. Untuk menghadiri wisuda di Petra dan di Unesa. Mungkin nanti akan diusahakan untuk bisa gathering relawan bersama beliau.

-----

Dari hasil urunan relawan yang hadir kemarin malam, terkumpul dana sekitar Rp. 250.000,- untuk kegiatan Sabtu besok. Kalau teman-teman juga mau menyumbang bisa kontak saudari Adha Anggraini.

------

Demikian laporan pertemuan kemarin malam. Kalau ada pertanyaan, kritik, dan saran, silakan.

Salam,
Relawan Rp.0,-